import { Grid } from '@mui/material'
import React from 'react'
import InputField from '../../../shared/components/InputField'


const RepositoryFilter = ({searchRepo, setSearchRepo}) => {
   
    return (
        <>
            <Grid container spacing={2} >
                <Grid item xs={12} md={4}>
                    <InputField
                        id="search"
                        label="Search"
                        placeholder="Search"
                        value={searchRepo}
                        onChange={(e) => setSearchRepo(e.target.value)}
                        sx={{ width: "100%", marginBottom: '1rem' }}
                    />
                </Grid>
            </Grid>
        </>
    )
}

export default RepositoryFilter