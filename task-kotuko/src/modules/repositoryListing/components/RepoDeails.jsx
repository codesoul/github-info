import React from 'react';
import Paper from '@mui/material/Paper';
import Typography from '@mui/material/Typography';
import Link from '@mui/material/Link';
import Grid from '@mui/material/Grid';

const RepoDetails = ({ repoDetails }) => {
  return (
    <Paper elevation={3} style={{ padding: '1rem' }}>
      <Grid container spacing={2}>
        <Grid item xs={12}>
          <Typography variant="body1">
            Repository Name:   {repoDetails.name}
          </Typography>
        </Grid>
        <Grid item xs={12}>
          <Typography variant="body1">
            Repository Details:  {repoDetails.description}
          </Typography>
        </Grid>
        <Grid item xs={12} sm={6}>
          <Typography variant="body1">
            Open Issues: {repoDetails.open_issues}
          </Typography>
        </Grid>
        <Grid item xs={12} sm={6}>
          <Typography variant="body1">
            Default Branch: {repoDetails.default_branch}
          </Typography>
        </Grid>
      </Grid>
    </Paper>
  );
};

export default RepoDetails;
