import React, {  useState } from 'react'
import RepositoryFilter from './components/RepositoryFilter'
import { Box, Paper } from '@mui/material'
import axios from 'axios'
import CustomizedTable from '../../shared/components/Table'
import { useDispatch } from 'react-redux'
import { setLoader } from '../../redux/redux-slices/repositorySlice'
import VisibilityIcon from '@mui/icons-material/Visibility';
import CustomizedDialogs from '../../shared/components/CustomModal'
import RepoDetails from './components/RepoDeails'
import { format } from 'date-fns';

const RepositoryListing = () => {

    const [repoData, setRepoData] = useState([]);
    const [totalData, setTotalData] = useState();
    const [searchRepo, setSearchRepo] = useState('angular');
    const [repoDetails, setRepoDetails] = useState();

    const [open, setOpen] = React.useState(false);

    const handleView = (rowData) => {
        setRepoDetails(rowData)
        setOpen(true);
    };
    const handleClose = () => {
        setOpen(false);
    };

    const dispatch = useDispatch();

    // to be customized according to the table functionality i.e sort 
    //this can be controlled by this headers
    const tableHeaders = [
        {
            label: 'Name',
            dataKey: 'name',
            sort: true
        },
        {
            label: 'Author',
            dataKey: 'avatar_url',
            Cell: ({ row }) => {
                console.log(row)
                return (
                    <Box>
                        <a href={row.owner.avatar_url}>
                            <img style={{ width: '40px', height: '40px' }} src={row.owner.avatar_url} />
                        </a>
                    </Box>
                );
            },
        },
        {
            label: 'Stars',
            dataKey: 'stargazers_count',
            sort: true
        },
        {
            label: 'Watchers',
            dataKey: 'watchers_count',
            sort: true
        },
        {
            label: 'Forks',
            dataKey: 'forks_count'
        },
        {
            label: 'Description',
            dataKey: 'description',
        },
        {
            label: 'Last Updated',
            dataKey: 'updated_at',
            Cell: ({ row }) => {
				return <span>{format(new Date(row.updated_at), 'dd-MM-yyyy')}</span>;
			},
        },
        {
            label: 'Action',
            dataKey: '',
            Cell: ({ row }) => {
                return (
                    <Box display='flex' justifyContent='center'>
                        <VisibilityIcon style={{ color: '#2196f3', cursor: 'pointer' }} onClick={()=>handleView(row)} />
                    </Box>
                );
            },
        }
    ]

    const getData = async ({ page, pageSize, searchQuery, order, orderBy }) => {
        dispatch(setLoader(true))
        try {
            const response = await axios.get('https://api.github.com/search/repositories', {
                params: {
                    q: searchQuery ? searchQuery : 'default',
                    sort: orderBy,
                    page: page,
                    per_page: pageSize,
                    order: order,
                },
            });
            setRepoData(response.data.items)
            setTotalData(response.data.total_count)
            dispatch(setLoader(false))
        } catch (error) {
            console.error('Error fetching data:', error);
        }
    };

    const fetchData = React.useCallback((page, pageSize, searchQuery, order, orderBy) => {
        getData(page, pageSize, searchQuery, order, orderBy)
    }, []);

    return (
        <>
            <Box
                sx={{
                    width: '100%',
                    background: '#FFFFFF 0% 0% no-repeat padding-box',
                    boxShadow: '0px 10px 30px #0000001A',
                    border: '0px 10px 30px #0000001A',
                    borderRadius: '10px',
                }}
            >
                <Paper
                    sx={{
                        padding: '3%'
                    }}>

                    <RepositoryFilter
                        searchRepo={searchRepo}
                        setSearchRepo={setSearchRepo}
                    />

                    <CustomizedTable
                        fetchData={fetchData}
                        headers={tableHeaders}
                        data={repoData}
                        totalData={totalData}
                        searchRepo={searchRepo}
                    />

                </Paper>
            </Box>

            <CustomizedDialogs header="Git Profile Detail" open={open} handleClose={handleClose}>
                <RepoDetails
                    repoDetails = {repoDetails}
                />
            </CustomizedDialogs> 
        </>
    )
}

export default RepositoryListing