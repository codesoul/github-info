import * as React from 'react';
import { useSelector } from 'react-redux';
import NavigationBar from './shared/components/navigationBar';
import RepositoryListing from './modules/repositoryListing/RepositoryListing';
import { Backdrop, CircularProgress } from '@mui/material';

const loaderStyle = {
  position: 'absolute',
  top: '50%',
  left: '50%',
  width: '100px',
  height: '100px'
};

function App() {
  const loader = useSelector((state) => state.repository.loader)

  return (
    <>
      <div>
        {loader &&
          <><Backdrop
            sx={{ color: '#fff', zIndex: (theme) => theme.zIndex.drawer + 1 }}
            open={loader}
          >
            <CircularProgress style={loaderStyle} />
          </Backdrop>
          </>
        }
        <NavigationBar />
        <RepositoryListing />
      </div>
    </>
  )
}

export default App
