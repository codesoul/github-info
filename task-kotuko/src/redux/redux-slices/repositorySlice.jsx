import { createSlice} from '@reduxjs/toolkit'

const initialState = {
    repositoryList: [],
    loader: false
}

export const repositorySlice = createSlice({
    name: 'repository',
    initialState,
    reducers: {
        setListingValues: (state, action) => {
            state.repositoryList = action.payload
        },
        setLoader: (state, action) => {
            state.loader = action.payload
        }
    },
})

export const { setListingValues , setLoader} = repositorySlice.actions

export default repositorySlice.reducer