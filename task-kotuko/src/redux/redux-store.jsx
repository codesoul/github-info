import { configureStore } from '@reduxjs/toolkit'
import repositoryReducer from './redux-slices/repositorySlice'

export const store = configureStore({
    reducer: {
        repository: repositoryReducer
    },
})