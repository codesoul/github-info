import { TextField } from '@mui/material'

const InputField = ({
  id,
  label,
  placeholder,
  value,
  variant = 'outlined',
  onChange,
  required = false,
  disabled = false,
  ...props
}) => {
  return (
    <TextField
      id={id}
      label={label}
      placeholder={placeholder}
      value={value}
      variant={variant}
      onChange={onChange}
      required={required}
      disabled={disabled}
      {...props}
  />
  )
}

export default InputField