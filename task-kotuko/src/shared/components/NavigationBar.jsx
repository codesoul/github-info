import { AppBar, IconButton, Toolbar, Typography } from '@mui/material'

const NavigationBar = () => {
    return (
        <AppBar position="static">
            <Toolbar variant="dense">
                <IconButton edge="start" color="inherit" aria-label="menu" sx={{ mr: 2 }}>
                    {/* <MenuIcon /> */}
                </IconButton>
                <Typography variant="h6" color="inherit" component="div">
                    Github
                </Typography>
            </Toolbar>
        </AppBar>
    )
}

export default NavigationBar