import * as React from 'react';
import { styled } from '@mui/material/styles';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell, { tableCellClasses } from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';
import { Box, TablePagination, TableSortLabel } from '@mui/material';
import PropTypes from 'prop-types';
import { visuallyHidden } from '@mui/utils';


const StyledTableCell = styled(TableCell)(({ theme }) => ({
    [`&.${tableCellClasses.head}`]: {
        backgroundColor: theme.palette.primary.main,
        color: theme.palette.common.white,
        minWidth: '100px',
        textAlign: 'center'
    },
    [`&.${tableCellClasses.body}`]: {
        fontSize: 14,
    },
}));

const StyledTableRow = styled(TableRow)(({ theme }) => ({
    '&:nth-of-type(odd)': {
        backgroundColor: theme.palette.action.hover,
    },
    // hide last border
    '&:last-child td, &:last-child th': {
        border: 0,
    },
}));


export default function CustomizedTable({ fetchData, headers, data, totalData, searchRepo }) {
    const [page, setPage] = React.useState(0);
    const [count, setCount] = React.useState();
    const [rowsPerPage, setRowsPerPage] = React.useState(10);
    const [orderBy, setOrderby] = React.useState()
    const [order, setOrder] = React.useState();


    const handleChangePage = function (e, page) {
        setPage(page);
    }

    const handleChangeRowsPerPage = function (e) {
        setRowsPerPage(parseInt(e.target.value, 10));
        setPage(0);
    }

    const createSortHandler = function (dataKey) {
        setOrder(order === 'asc' ? 'desc' : 'asc')
        setOrderby(dataKey)
        setPage(0);
    }

    React.useEffect(() => {
        fetchData({
            page: page,
            pageSize: rowsPerPage,
            searchQuery: searchRepo,
            order: order,
            orderBy: orderBy
        })
    }, [page, rowsPerPage, searchRepo, order])


    return (
        <TableContainer component={Paper}>
            <Table sx={{ minWidth: 700 }} aria-label="customized table">

                <TableHead>
                    <TableRow>
                        {
                            headers.map((header) =>
                                <>
                                    <StyledTableCell>
                                        {header.sort && <TableSortLabel
                                            active={true}
                                            direction={orderBy === header.dataKey ? order : 'asc'}
                                            onClick={()=>createSortHandler(header.dataKey)}
                                        >
                                            {orderBy === header.dataKey ? (
                                                <Box component="span" sx={visuallyHidden}>
                                                    {order === 'desc' ? 'sorted descending' : 'sorted ascending'}
                                                </Box>
                                            ) : null}
                                        </TableSortLabel>}
                                        {header.label}
                                        
                                        </StyledTableCell>
                                </>)
                        }
                    </TableRow>
                </TableHead>

                <TableBody>
                    {data.map((row) => (
                        <StyledTableRow key={row.id}>
                            {headers.map((key) => (
                                <React.Fragment key={key.dataKey}>
                                    {key.Cell ? (
                                        <StyledTableCell align="center">{key.Cell({ row })}</StyledTableCell>
                                    ) : (
                                        <StyledTableCell align="center">{row[key.dataKey]}</StyledTableCell>
                                    )}
                                </React.Fragment>
                            ))}
                        </StyledTableRow>
                    ))}
                </TableBody>
            </Table>

            <TablePagination
                component="div"
                count={totalData}
                page={page}
                onPageChange={handleChangePage}
                rowsPerPage={rowsPerPage}
                onRowsPerPageChange={handleChangeRowsPerPage}
            />

        </TableContainer>
    );
}

CustomizedTable.defaultProps = {
    headers: [],
    data: [],
    fetchData: () => { },
    totalData: 0
}

CustomizedTable.protoTypes = {
    headers: PropTypes.array.isRequired,
    data: PropTypes.array.isRequired,
    fetchData: PropTypes.func.isRequired,
    totalData: PropTypes.number.isRequired
}