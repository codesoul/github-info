import { FormControl, InputLabel, MenuItem, Select } from "@mui/material";

const SelectField = ({
    label,
    value,
    onChange,
    options = [],
    variant = 'outlined',
    required = false,
    disabled = false,
    ...props
}) => {
    return (
        <FormControl 
            variant={variant} 
            required={required} 
            disabled={disabled} 
            fullWidth 
            {...props}>
                    {label && <InputLabel>{label}</InputLabel>}
                    <Select value={value} onChange={onChange} label={label}>
                        {options.map((option) => (
                            <MenuItem key={option.value} value={option.value}>
                                {option.label}
                            </MenuItem>
                        ))}
                    </Select>
        </FormControl>
    );
};

export default SelectField