
Repository Link: https://gitlab.com/codesoul/github-info

Reusable Components Created Under Shared folder: 

    1. Table.jsx for reusable table 
    2. CustomModal.jsx for reusable Modal component
    and so on.

NPM packages used are: 
    "axios": "^1.4.0",
    "react": "^18.2.0",
    "react-dom": "^18.2.0",
    "react-redux": "^8.1.1",
    "react-router-dom": "^6.14.2"
    "vite": "^4.4.5"
